import { IMPORTANT_VALUE, logInCaps, Dog } from './import-from-me.js';

function createTriviaHtml(categoryName, question, answer) {
    return `
    <div>
        <h3>${categoryName}</h3>
        <p><b>Question: </b>${question}</p>
        <p><b>Answer: </b>${answer}</p>
    </div>
    `
}

/*
window.addEventListener('DOMContentLoaded', async () => {
    console.log('DOM content fully loaded and parsed.');


    const url = "https://jservice.xyz/api/random-clue";

    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);

            const categoryName = data.category.title;
            const question = data.question;
            const answer = data.answer;
            const mainTag = document.querySelector("main");

            mainTag.innerHTML = createTriviaHtml(categoryName, question, answer);

        } else {
            throw new Error('Response not ok');
        }
    } catch (error) {
        console.error('error', error);
    }
}
);
*/

// extract the data input from user
const createClueButtom = document.getElementById("create-clue");
createClueButtom.addEventListener("click", async () => {
    const question = document.getElementById("question").value;
    const answer = document.getElementById('answer').value;
    const value = Number.parseInt(document.getElementById('value').value);

    const data = {
        question: question,
        answer: answer,
        value: value,
        categoryId: 1485,
    };

    const url = "https://jservice.xyz/api/clues";
    const fetchOptions = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
        const responseData = await response.json();

        const category = responseData.category.title;
        const question = responseData.question;
        const answer = responseData.answer;
        const html = createTriviaHtml(category, question, answer);

        const newClueTag = document.getElementById('new-clue');
        newClueTag.innerHTML = html;
    }


})
// create a json to store the data
// send the data to the server
// get the data response from the server, then output it in HTML






console.log('IMPORTANT_VALUE is', IMPORTANT_VALUE);
logInCaps('This is a message.');

const dog = new Dog('Fido');
const dogWords = dog.speak();
console.log(dogWords);

console.log('main.js is loaded');
